import { environment } from "src/environments/environment"
const base_url= environment.base_url;
export class Usuario {
    constructor(
        public nombre:string,
        public email:string,
        public password?:string,
        public img?:string,
        public role?:string,
        public google?:boolean,
        public uid?:string,
    ){
        
    }
    // http://localhost:3000/api/upload/usuarios/38045ecc-35fa-4ea6-8583-bb5b56fb8e74.jpg
  get urlImagen(){
    if(this.img?.includes('https')){
        return this.img;
    }
    if(!this.img){
        return `${base_url}/upload/usuarios/no.img`
    }else{
        return `${base_url}/upload/usuarios/${this.img}`
    }
  }
}