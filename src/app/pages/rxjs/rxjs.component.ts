import { Component, OnDestroy } from '@angular/core';
import { Observable, Subscription, filter, interval, map, take } from 'rxjs';

@Component({
  selector: 'app-rxjs',
  templateUrl: './rxjs.component.html',
  styles: [
  ]
})
export class RxjsComponent implements OnDestroy{
  public intervalSubs!: Subscription;
  constructor(){
    
    this.intervalSubs= this.retornaIntervalo()
        .subscribe(console.log);
    // this.retornaObserbable().subscribe(valor=>{
    //   console.log('subs: ', valor)
      
    // },(error)=>{
    //   console.log('hay un error en el observador', error);
    // },()=>{
    //   console.info('obs terminado');
    // });
  }
  ngOnDestroy(): void {
    this.intervalSubs.unsubscribe();
  }

  retornaIntervalo(): Observable<number>{
    return interval(500)
                      .pipe(
                        //take(10),
                        map(valor=>valor+1),
                        filter(valor=>(valor%2==0)?true:false),                
                      )
  }
  

  retornaObserbable(){
    let i=-1;
    const obs$= new Observable<number>(observer=>{
      const intervalo = setInterval(()=>{
        i++;
        observer.next(i);
        if(i==4){
          clearInterval(intervalo);
          observer.complete();
        }
        if(i==2){
          observer.error('i llego a 2')
        }
      },1000)
    });
    return obs$;
  }
}
