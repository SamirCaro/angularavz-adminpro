import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

import { Usuario } from 'src/app/models/usuario.model';
import { FileUploadService } from 'src/app/services/file-upload.service';
import { UsuarioService } from 'src/app/services/usuario.service';


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styles: [
  ]
})
export class PerfilComponent implements OnInit {
  public formPerfil!:FormGroup;
  public usuario!:Usuario;
  public imagenSubir!:File;
  public imagenTemporal:any=null;

  constructor(private fb:FormBuilder,
              private usuarioService:UsuarioService,
              private fileUploadService:FileUploadService){
    this.usuario=usuarioService.usuario;
  }

  ngOnInit(): void {
    this.formPerfil = this.fb.group({
      nombre:[this.usuario.nombre,[Validators.required]],
      email:[this.usuario.email,[Validators.required,Validators.email]]
    })
  }

  actualizarPerfil(){
    // console.log(this.formPerfil.value);
    this.usuarioService.actualizarUsuario(this.formPerfil.value)
                       .subscribe((resp:any)=>{
                        console.log(resp);
                          this.usuario.nombre=resp.usuario.nombre;
                          this.usuario.email=resp.usuario.email;
                          Swal.fire('Guardado','Usuario Actualizado','success');
                       },(err)=>{
                        //  console.log(err.error.msg);
                          Swal.fire('Error',err.error.msg,'error')
                       })
  }

  cambiarImagen(event:any){
    // console.log(event.target.files[0]);
    this.imagenSubir=event.target.files[0];
    if(!this.imagenSubir){
      this.imagenTemporal=null;
      return
    }
    const reader = new FileReader();
    reader.readAsDataURL(this.imagenSubir);
    reader.onloadend = ()=>{
      // console.log(reader.result);
      this.imagenTemporal=reader.result;
    }
  }

  subirImagen(){
    // console.log('listo para subir imagen');
    const id=this.usuario.uid || ''
    this.fileUploadService.actualizarFoto(this.imagenSubir,'usuarios',id)
        .subscribe((resp:any)=>{
          // console.log(resp.nombreArchivo);
          this.usuario.img=resp.nombreArchivo;
          Swal.fire('Guardado','Imagen Actualizada','success');
        }, (err)=>{
          Swal.fire('Error',err.error.msg,'error')
        })

  }



}
