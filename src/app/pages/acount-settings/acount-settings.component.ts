import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'app-acount-settings',
  templateUrl: './acount-settings.component.html',
  styles: [
  ]
})
export class AcountSettingsComponent implements OnInit {
  
  public linkThema = document.querySelector('#theme');
  public elementsThema!:NodeListOf<Element>; 

  constructor(private settingsService:SettingsService){}

  ngOnInit()  {
   this.elementsThema=document.querySelectorAll('.selector');
    this.chequeoThema();
  }

cambioThema(thema:string){
  this.settingsService.cambioThema(thema);
  this.chequeoThema()  
}

chequeoThema(){
  const urlLinkThema=this.linkThema?.getAttribute('href');
  this.elementsThema.forEach(element=>{
    element.classList.remove('working');
    const thema= element.getAttribute('data-theme');
    const urlElement=`./assets/css/colors/${thema}.css`
    if(urlLinkThema===urlElement){
      element.classList.add('working');
    }
    

  })
}
}
