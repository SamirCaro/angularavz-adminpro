import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-promesas',
  templateUrl: './promesas.component.html',
  styles: [
  ]
})
export class PromesasComponent implements OnInit {
  ngOnInit(): void {
    this.getUsuarios().then(usuarios=>{
      console.log(usuarios);
    });
    // const promesa = new Promise((resolve,reject)=>{
    //   if(false){
    //     resolve('Hola mundo')
    //   }else{
    //     reject('Algo Salio Mal')
    //   }
    // });
    // promesa.then((mensaje)=>{
    //   console.log(mensaje);

    // }).catch(error=>{
    //   console.log('no se resolvio y el mensaje es: ',error);
    // })

  
  }

  getUsuarios(){
    const promesa = new Promise(resolve=>{
      fetch('https://reqres.in/api/users').then(resp=>{
      resp.json().then(body=>{
       resolve(body.data); 
      })
    });
    })
    return promesa;
  }

 


}
