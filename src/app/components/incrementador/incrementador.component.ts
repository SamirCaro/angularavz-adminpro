import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styles: [
  ]
})
export class IncrementadorComponent implements OnInit {
  
  @Input('valor') progreso:number = 0;
  @Input() buttonClass:string="btn-primary";
  @Output('valor') nuevoValor:EventEmitter<number> = new EventEmitter();

  ngOnInit() {
   this.buttonClass=`btn h-100 ${this.buttonClass}`;
  }

  public valorInput:number=this.progreso;


  cambioProgreso(valor:number){
    
    if(this.progreso >=100 && valor>=0){
      this.progreso=100;
    }else{
      if(this.progreso <=0 && valor<=0){
        this.progreso=0
      }else{
        this.progreso=this.progreso+valor;
      }
    }
    this.nuevoValor.emit(this.progreso);
  }

  cambioInput(valor:number){
    this.valorInput=valor;
    if(valor > 100){
      this.progreso=0
    }else{
      if(valor <= 0){
        this.progreso=0
      }else{
        this.progreso=valor;
      }  
    }
    this.nuevoValor.emit(this.progreso);
  }

}
