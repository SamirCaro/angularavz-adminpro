import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private linkThema = document.querySelector('#theme');

  constructor() {
  }
  cargarThema(){
    const url=localStorage.getItem('thema') || './assets/css/colors/green-dark.css'
    this.linkThema?.setAttribute('href',url);
  }

  cambioThema(thema:string){
    const url=`./assets/css/colors/${thema}.css`
    this.linkThema?.setAttribute('href',url);
    localStorage.setItem('thema',url);
  }
}
