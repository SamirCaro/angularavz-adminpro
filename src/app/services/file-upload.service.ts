import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

const base_url = environment.base_url;

@Injectable({
  providedIn: 'root'
})


export class FileUploadService {
  // http://localhost:3000/api/upload/usuarios/64caeae17ac92a38111395bd

  constructor(private http:HttpClient) {  
  }

  actualizarFoto(
    foto:File,
    tipo:'usuarios'|'medicos'|'hospitales',
    id:string
  ){
    const url=`${base_url}/upload/${tipo}/${id}`;
    const token= localStorage.getItem('token') || '';
    const formData = new FormData();
    formData.append('imagen', foto);
    // console.log(foto);
    // console.log(tipo);
    // console.log(id);
    return this.http.put(url,formData,{headers:{
      'x-token':token
    }})
  }
}
