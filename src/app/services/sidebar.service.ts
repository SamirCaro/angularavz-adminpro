import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SidebarService {
  public menu:any[]=[
    {
      titulo:'Dashboard!!!',
      clase:'mdi mdi-gauge',
      submenu:[
        {
          titulo:'main',
          path:'/'
        },
        {
          titulo:'Progress',
          path:'progress'
        },
        {
          titulo:'Promesas',
          path:'promesas'
        },
        {
          titulo:'Rxjs',
          path:'rxjs'
        },
        {
          titulo:'Grafica',
          path:'grafica1'
        },
        
      ]
    }
  ]
  constructor() { }
}
