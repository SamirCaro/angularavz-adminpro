import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, map, tap } from 'rxjs/operators'
import { RegisterForm } from '../interfaces/register-form.interface';
import { environment } from 'src/environments/environment';
import { LoginForm } from '../interfaces/login-form.interface';
import { Observable, of } from 'rxjs';
import { Usuario } from '../models/usuario.model';

declare const google:any;


const base_url= environment.base_url;

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {
  public usuario!:Usuario;

  constructor(private http:HttpClient) { }

  get uid(){
    return this.usuario.uid || '';
  }

  get token (){
    return localStorage.getItem('token') || '';
  }

  logout(){
    localStorage.removeItem('token');
    google.accounts.id.revoke('caro.arqconstrucciones@gmail.com',()=>{

    })

  }

  validarToken():Observable<boolean>{
    const token = localStorage.getItem('token') || '';
    // console.log(token);
    return this.http.get(`${base_url}/login/renew`,{ headers:{'x-token':token}})
                    .pipe(
                      map((resp:any)=>{
                        const {email,google,img='',nombre,role,uid}= resp.usuario;
                        // console.log(email);
                        this.usuario=new Usuario(nombre,email,'',img,role,google,uid);
                        console.log(this.usuario);
                        localStorage.setItem('token',resp.token);
                        return true
                      }),
                      catchError(error=>
                        of(false)
                      )
                    )
  }

  crearUsuario(formData:RegisterForm){
    // console.log('creando usuario', formData);
    return this.http.post(`${base_url}/usuarios`,formData)
                    .pipe(
                        tap((resp:any)=>{
                            localStorage.setItem('token',resp.token)
                        })
                    )
  }

  actualizarUsuario(data:{nombre:string,email:string,role:string}){
    data={
      ...data,
      role:'USER-ROLE'
    }
    return this.http.put(`${base_url}/usuarios/${this.uid}`,data,{
      headers:{'x-token': this.token}
    })
  }

  loginUsuario (formData:LoginForm){
    return this.http.post(`${base_url}/login`, formData)
                    .pipe(
                      tap((resp:any)=>{
                        localStorage.setItem('token',resp.token)
                      })
                    )
  }

  loginGoogle(token:string){
    return this.http.post(`${base_url}/login/google`, {token})
                    .pipe(
                      tap((resp:any)=>{
                        // console.log(resp);
                        localStorage.setItem('token',resp.token)

                      })
                    )
  }
}
