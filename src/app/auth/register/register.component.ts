import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import Swal from 'sweetalert2'

import { UsuarioService } from 'src/app/services/usuario.service';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'
  ]
})
export class RegisterComponent {
  public formSubmitted= false;

  public registerForm = this.fb.group({
    nombre:['Adolfo Caro', Validators.required],
    email:['adolfo@gmail.com', [Validators.required, Validators.email]],
    password:['123456', Validators.required],
    password2:['123456', Validators.required],
    terminos:[false, Validators.required]
  }, {Validators: this.passwordIguales('password','password2')})

  constructor(private fb:FormBuilder,
              private usuarioService:UsuarioService,
              private router:Router){
  }

  crearUsuario(){
    this.formSubmitted=true;
    // console.log(this.registerForm.value);
    
    if(this.registerForm.valid && this.registerForm.get('terminos')?.value){
      console.log(this.registerForm.value);
      this.usuarioService.crearUsuario(this.registerForm.value)
                         .subscribe(resp=>{
                          console.log('usuario creado en la DB');
                          console.log(resp);
                          this.router.navigateByUrl('/');
                         },(err)=>{
                          Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: err.error.msg,
                        
                          })
                         })
    }else{
      console.log('Formuario invalido');
      return
    }
  }

  validacionCampo(campo:string):boolean{
      
      if(this.registerForm.get(campo)?.invalid && this.formSubmitted){
        return true;
      }else{
        return false;
      }
  }
  validacionCampoTerminos(){
    if(!this.registerForm.get('terminos')?.value && this.formSubmitted){
      return true
    }else{
      return false
    }
  }

  validacionPassword(){
    const pass1= this.registerForm.get('password')?.value;
    const pass2= this.registerForm.get('password2')?.value;
    if((pass1!==pass2)&& this.formSubmitted){
      return true
    }else{
      return false
    }
  }

  passwordIguales(passw1:string,passw2:string){
    return (formGroup:FormGroup)=>{
      const passw1Control= formGroup.get(passw1);
      const passw2Control= formGroup.get(passw2);
      if (passw1Control?.value===passw2Control?.value){
        passw2Control?.setErrors(null)
      } else{
        passw2Control?.setErrors({NoIguales:true})
      }
    }
  }
}
