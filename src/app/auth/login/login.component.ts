import { AfterViewInit, Component, ElementRef, ViewChild } from '@angular/core';
import { FormBuilder,FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';
import Swal from 'sweetalert2';

declare const google:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'
  ]
})
export class LoginComponent implements AfterViewInit {
  
  @ViewChild('googleBtn') googleBtn!:ElementRef;

  public formSubmitted= false;
  public loginForm:FormGroup = this.fb.group({
    email:[localStorage.getItem('email') || '', [Validators.required, Validators.email]],
    password:['', Validators.required],
    remember:[false]
  });

 

  constructor(private router:Router,
              private fb:FormBuilder,
              private usuarioService:UsuarioService){}


  ngAfterViewInit(){
    this.googleInit()
  }

  googleInit(){
    google.accounts.id.initialize({
      client_id: "382429250967-hq6te02vsqt5hh58geqp478mcu1t20b9.apps.googleusercontent.com",
      callback:(response:any)=> this.handleCredentialResponse(response)
    });
    google.accounts.id.renderButton(
      // document.getElementById("buttonDiv")
      this.googleBtn.nativeElement, {
          theme: "outline",
          size: "large"
      } // customization attributes
  );
  }

  handleCredentialResponse(response:any) {
    // console.log("Encoded JWT ID token: " + response.credential);
    this.usuarioService.loginGoogle(response.credential)
                       .subscribe(resp=>{
                          this.router.navigateByUrl('/');
                       })

    
}

  login(){
    // console.log(this.loginForm.value);
    if(this.loginForm.get('remember')?.value){
      localStorage.setItem('email', this.loginForm.get('email')?.value);
    }else{
      localStorage.removeItem('email');
    }
    this.usuarioService.loginUsuario(this.loginForm.value)
                        .subscribe(resp=>{
                          // console.log(resp);
                          this.router.navigateByUrl('/');
                        },(err)=>{
                          Swal.fire({
                            icon: 'error',
                            title: 'Error',
                            text: err.error.msg,
                        
                          })
                         })
    
  }
}
