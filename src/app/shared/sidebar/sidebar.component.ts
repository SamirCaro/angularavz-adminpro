import { Component } from '@angular/core';
import { SidebarService } from '../../services/sidebar.service';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styles: [
  ]
})
export class SidebarComponent  {
  public menuItems:any[];
  public usuario;
  constructor(private sidebarService:SidebarService,
              private usuarioService:UsuarioService){
    this.menuItems=sidebarService.menu;
    this.usuario=this.usuarioService.usuario;
    
  }
  logout(){
    this.usuarioService.logout();
  }
  
  


}
